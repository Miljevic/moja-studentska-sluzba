package vp.spring.rcs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Student;
import vp.spring.rcs.service.StudentService;

@RestController
@RequestMapping(value = "/api/students")
public class StudentController {

	@Autowired
	StudentService studentService;
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<Student>> findAll(){
		List<Student> studenti = studentService.findAll();
		return new ResponseEntity<>(studenti, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<Student> save(@RequestBody Student newStudent){
		Student student = studentService.save(newStudent);
		return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Student> getById(@PathVariable Long id) {
		Student student = studentService.findOne(id);
		
		if (student != null) {
			return new ResponseEntity<>(student, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		

	}
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		
		
			studentService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		
	}

	
}
