import { Component, OnInit } from '@angular/core';
import { Http, Response, RequestOptions,
         Headers, URLSearchParams } from '@angular/http';

import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { Student } from 'app/student.model';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {

  studenti: any = [];
  student: Student = {
    ime: '',
    prezime: '',
    brojIndeksa: ''
  };

  ngOnInit(): void {
  }

  constructor(private http: Http) {
    this.loadData();
  }

  loadData() {
    const o: any = this.http.get('api/students');
    o.subscribe( (data: Response) => {
      this.studenti = data.json();
    });
  }

  saveStudent() {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const o: Observable<Response> = this.http.post('api/students', JSON.stringify(this.student), options);
    o.subscribe((data: Response) => {
      this.loadData();
    });
  }
  delete(n: any) {
    this.http.delete(`api/students/${n.id}`).subscribe(data =>{
      console.log(data);
      this.loadData();
    });
  }


}
