import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { Student } from 'app/student.model';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
student: Student;

  constructor(private route: ActivatedRoute, private http: Http) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.http.get(`api/students/${id}`).subscribe(response => {
        this.student = response.json();
      });
    });
    
    
  }
  delete(n: any) {
    this.http.delete(`api/students/${n.id}`).subscribe(data =>{
      console.log(data);
     
    });
  }



  ngOnInit() {
  }

}
