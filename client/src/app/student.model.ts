export class Student {
    ime: string;
    prezime: string;
    brojIndeksa: string;
    constructor(ime: string, prezime: string, brojIndeksa: string) {
        this.ime = ime;
        this.prezime = prezime;
        this.brojIndeksa = brojIndeksa;
    }
}
